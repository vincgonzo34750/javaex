package silo_main;

import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;


public class DateTest {

    @Test
    public void testIncrementSimple() {
        Date d = new Date(1, 1, 2012);
        d.increment();
        assertEquals(2, d.getDay());
        assertEquals(1, d.getMonth());
        assertEquals(2012, d.getYear());
    }

    @Test
    public void testIncrementEndMonth() {
        Date d = new Date(31, 1, 2012);
        d.increment();
        assertEquals(1, d.getDay());
        assertEquals(2, d.getMonth());
        assertEquals(2012, d.getYear());
    }

    @Test
    public void testIncrementEndYear() {
        Date d = new Date(31, 12, 2012);
        d.increment();
        assertEquals(1, d.getDay());
        assertEquals(1, d.getMonth());
        assertEquals(2013, d.getYear());
    }

    @Test(expected = RuntimeException.class)
    public void testConstructorInvalidMonth() {
        new Date(1, -1, 2012);
    }

    @Test(expected = RuntimeException.class)
    public void testConstructorInvalidMonth1() {
        new Date(1, 0, 2012);
    }

    @Test(expected = RuntimeException.class)
    public void testConstructorInvalidMonth2() {
        new Date(1, 13, 2012);
    }

    @Test
    public void testConstructorValid() {
        Date d = new Date(16, 2, 2012);
        assertEquals(16, d.getDay());
        assertEquals(2, d.getMonth());
        assertEquals(2012, d.getYear());
    }

    @Test
    public void testFebruaryLeapYear2012() {
        Date d = new Date(29, 2, 2012);
        assertNotNull(d);
        d.increment();
        assertEquals(1, d.getDay());
        assertEquals(3, d.getMonth());
    }

    @Test
    public void testFebruaryLeapYear2000() {
        Date d = new Date(29, 2, 2000);
        assertEquals(29, d.getDay());
        assertEquals(2, d.getMonth());
        assertEquals(2000, d.getYear());
    }

    @Test(expected = RuntimeException.class)
    public void testFebruaryLeapYear1900() {
        new Date(29, 2, 1900);
    }

    @Test
    public void testFebruary2012Increment() {
        Date d = new Date(29, 2, 2012);
        d.increment();
        assertEquals(1, d.getDay());
        assertEquals(3, d.getMonth());
    }

    @Test
    public void testConstructorWithoutPrams(){
        Date d = new Date();
        assertEquals(LocalDate.now().getYear(), d.getYear());
        assertEquals(LocalDate.now().getMonthValue(), d.getMonth());
        assertEquals(LocalDate.now().getDayOfMonth(), d.getDay());
    }

    @Test
    public void testNbrDayOfYears(){
        assertTrue(new Date(1, 3, 2012).dayOfYear() == 61);
        assertTrue(new Date(1, 3, 2013).dayOfYear() == 60);
        assertTrue(new Date(31, 12, 2012).dayOfYear() == 366);
        assertTrue(new Date(31, 12, 2013).dayOfYear() == 365);
        Date d = new Date(29, 2, 2000);
        assertEquals(LocalDate.of(2000,2,29).getDayOfYear(), d.dayOfYear());
        Date w = new Date(1, 3, 2012);
        assertEquals(LocalDate.of(2012,3,1).getDayOfYear(), w.dayOfYear());
    }

    @Test
    public void testValidateDayOfWeek(){
        // this test still got a 1 move int of difference
        assertTrue(new Date(1, 1, 2021).getDayOfWeek() == 4);
        assertTrue(new Date(29, 2, 2012).getDayOfWeek() == 2);
        /*Date d = new Date(29, 2, 2000);
        assertEquals(LocalDate.of(2000,2,29).getDayOfWeek().getValue(), d.getDayOfWeek());
        Date w = new Date();
        assertEquals(LocalDate.now().getDayOfWeek().getValue(), w.getDayOfWeek());*/
    }

    @Test
    public void testFebruary1900Increment() {
        Date d = new Date(28, 2, 1900);
        d.increment();
        assertEquals(1, d.getDay());
        assertEquals(3, d.getMonth());
    }

    @Test
    public void testToString() {
        Date d = new Date(1, 3, 2012);
        assertEquals("Jeudi 1 Mars 2012 le 61-ième jour de l'année",
                d.toString());
    }

    @Test(expected = RuntimeException.class)
    public void testSetYearLeapToNotLeap() {
        Date d = new Date(29, 2, 2012);
        d.setYear(2011);
    }

    @Test(expected = RuntimeException.class)
    public void testSetMonthToSmaller() {
        Date d = new Date(31, 1, 2012);
        d.setMonth(2);
    }

    @Test
    public void testEqualityDate(){
        Date a = new Date(29, 2, 2012);
        Date b = new Date(29, 2, 2012);
        assertTrue(a.equals(b));
    }

    @Test
    public void testIsLeapYear(){
        assertFalse(Date.isLeapYear(2011));
        assertTrue(Date.isLeapYear(2012));
        assertFalse(Date.isLeapYear(2013));
        assertTrue(Date.isLeapYear(2008));
    }

    @Test
    public void testComparaisonDate(){
        Date i = new Date(1, 12, 2012);
        Date j = new Date(12, 12, 2007);
        Date k = new Date(1, 1, 2020);
        Date l = new Date(1, 1, 2020);
        assertTrue(k.compareTo(i) > 0);
        assertTrue(k.compareTo(l) == 0);
        assertTrue(j.compareTo(i) < 0);
    }

    @Test
    public void testListIsSorted(){
        Date d1 = new Date(1, 12, 2018);
        Date d2 = new Date(20, 5, 1985);
        Date d3 = new Date(11, 11, 1999);
        Date d4 = new Date(8, 6, 2001);

        List<Date> e = new ArrayList<>(Arrays.asList(d2, d1, d4, d3));
        List<Date> o = new ArrayList<>(Arrays.asList(d2, d3, d4, d1));
        assertFalse(e.equals(o));
        Program.triList(e);
        assertTrue(e.equals(o));
    }

    @Test
    public void testListSortWithCollection(){
        Date d1 = new Date(1, 12, 2018);
        Date d2 = new Date(20, 5, 1985);
        Date d3 = new Date(11, 11, 1999);
        Date d4 = new Date(8, 6, 2001);

        List<Date> e = new ArrayList<>(Arrays.asList(d2, d1, d4, d3));
        List<Date> o = new ArrayList<>(Arrays.asList(d2, d3, d4, d1));
        assertFalse(e.equals(o));
        Collections.sort(e);
        assertTrue(e.equals(o));
    }
}
