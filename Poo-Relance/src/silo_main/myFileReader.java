package silo_main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;


public class myFileReader {
    private static File fl;
    private String name;
    public static Map<String, Integer> map;

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Enter a file name to explore :");
        Scanner scan = new Scanner(System.in);
        myFileReader h = new myFileReader(scan.nextLine());

        //h.initMapOne();
        h.displayMaxIterationWords();

        copyFile("hamlet.txt", "test.txt");
    }

    public myFileReader(String nameFile){
        init(nameFile);
        this.name = nameFile;
    }

    public myFileReader(myFileReader paste){
        this(paste.name);
    }

    public static void copyFile(String cp, String pst) throws FileNotFoundException{
        try (Scanner sc = new Scanner(new File(cp))) {
            try (PrintWriter out = new PrintWriter(new File(pst))) {
                while(sc.hasNextLine()) {
                    String line = sc.nextLine();
                    out.println(line);
                }
            }
        }
    }

    private void init(String name){
        this.fl = new File(name + ".txt");
        try {
            Scanner sc = new Scanner(fl);
            sc.useDelimiter("[\\p{Punct}\\s’]+");
            this.map = new TreeMap<String, Integer>();
            while (sc.hasNext()) {
                String mot = sc.next().toLowerCase();
                int inter = this.map.getOrDefault(mot, 0);
                this.map.put(mot, ++inter);
            }
            if(sc != null)
                sc.close();
        }catch (FileNotFoundException except) {
            except.printStackTrace();
        }finally{
        }
    }

    private void displayMaxIterationWords(){
        for(Map.Entry<String, Integer> entry : map.entrySet())
            if(entry.getValue() >= 300 && entry.getKey().length() > 2){
                System.out.printf("Le mot %s ce trouve %s fois dans la collection. | ", entry.getKey(), entry.getValue());
            }
        System.out.println("\n\n");
    }

    private void initMapOne(){
        for(Map.Entry<String, Integer> entry : map.entrySet())
            if(entry.getValue() == 1){
                System.out.printf("Le mot %s ce trouve %s fois dans la collection. | ", entry.getKey(), entry.getValue());
            }
        System.out.println("\n\n");
    }

    public static void readMap(){
        for(Map.Entry<String, Integer> entry : map.entrySet())
            System.out.printf(" | Le mot %s ce trouve %s fois dans la collection. | ", entry.getKey(), entry.getValue());
    }

    public static void searchWordInMap(){
        Scanner input = new Scanner(System.in);
        String motCherche = "";
        do {
            System.out.print("\n\nChoisissez un mot (Enter pour terminer): ");
            try {
                motCherche = input.nextLine();

                if(!map.containsKey(motCherche))
                    System.out.println("Ce mot n'est pas dans la collection.");
                else
                    System.out.println("Ce mot compte " + map.get(motCherche) + " iteration" );
            }catch(InputMismatchException i){
                System.out.println("Vous n'avez pas rentrer un mot valide, réessayez.");
            }

        } while (!motCherche.isEmpty());
    }

}
