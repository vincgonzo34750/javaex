package silo_main;

import java.time.LocalDate;

public class Personn  implements Comparable<Personn> {

    private String firstName;
    private String lastName;
    private Date dateOfBirth;

    public Personn(String firstName, String lastName, int day, int month, int year){
        this.setFirstName(firstName);
        this.setLastName(lastName);
        dateOfBirth = new Date(day, month, year);
    }

    public int getAge(){
        LocalDate today = LocalDate.now();
        int day = today.getDayOfMonth();
        int month = today.getMonthValue();
        int year = today.getYear();
        // calc if day of current year is birthday
        int diff = (month < dateOfBirth.getMonth() ||
                month == dateOfBirth.getMonth() && day < dateOfBirth.getDay())? 1 : 0;

        return (year - dateOfBirth.getYear()) - diff;
    }

    public int getAgeAt(){
      return this.getAgeAt(new Date());
    }

    public int getAgeAt(Date a){
        if(a != null){
            int diff = (a.getMonth() < dateOfBirth.getMonth() ||
                    a.getMonth() == dateOfBirth.getMonth() && a.getDay() < dateOfBirth.getDay())? 1 : 0;

            return (a.getYear() - dateOfBirth.getYear()) - diff;
        }else
            throw new RuntimeException("Bad Date passed into function.");
    }

    public String getFirstName(){ return this.firstName; }

    public String getLastName(){ return this.lastName; }

    public void setLastName(String lastName){ this.lastName = lastName; }

    public void setFirstName(String firstName){ this.firstName = firstName; }

    public String toString(){
        return this.getFirstName() + " " + this.getLastName() + " né le : " + dateOfBirth;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean equals(Object o){
        if(o instanceof Personn){
            Personn p = (Personn) o;
            return p.getLastName() == this.getLastName() && p.getFirstName() == this.getFirstName()
                    && this.compareTo(p) == 0;
        }else
            return false;
    }
    
    public int compareTo(Personn o){
        return -this.dateOfBirth.compareTo(o.dateOfBirth);
    }
}
