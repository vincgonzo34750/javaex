package silo_main;

import java.util.List;
import java.util.Random;

public class Program {

    public <E extends Comparable<E>> int pos(List<E> m, E t){
        int k = 0;
        while(t.compareTo(m.get(k)) < 0)
            ++k;
        return k;
    }

    public static <E extends Comparable<E>> void triList(List<E> e){
        for (int i = 0; i < e.size(); i++) {
            for (int j = i + 1; j < e.size(); j++) {
                if(e.get(j).compareTo(e.get(i)) < 0){
                    E tmp = (E) e.get(i);
                    e.set(i, e.get(j));
                    e.set(j, tmp);
                }
            }
        }
    }

    public static <E extends Comparable<E>> void shuffle(List<E> e){
        Random rand = new Random();
        for (int i = 0; i < e.size(); i++) {
            int m = rand.nextInt(e.size());
            E tmp = e.get(m);
            e.set(m, e.get(i));
            e.set(i, e.get(m));
        }
    }

    public static <E extends Comparable<E>> void renderList(List<E> list){
        System.out.println("------- Date List ---------------");
        for (int i = 0; i < list.size(); i++) {
            System.out.println("--"+ list.get(i) +"--");
        }
        System.out.println("---------------------------------");
    }
}
