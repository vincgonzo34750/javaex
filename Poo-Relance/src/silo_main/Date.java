package silo_main;

import java.io.IOException;
import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Date implements Comparable<Date> {
     /*
        * Ecrire une classe Date. Ses attributs publics sont trois entiers : day, month et year.
            Cette classe possède les méthodes publiques suivantes :
            — public Date(day, month, year), le constructeur.
            — public void increment(), qui passe la date au jour suivant.
            — public int dayOfYear() qui renvoie le numéro du jour dans l’année.
            — public int dayOfWeek() qui renvoie le numéro du jour dans la semaine. Utilisez la congruence de Zeller
            (voir page suivante).
            — public String toString() qui transforme la date en chaîne de caractère. Par exemple, “Lundi 2 novembre
            2009”.
            — public void prettyPrint() qui affiche la date sur la sortie standard.
            Ajouter les méthodes qui vous semblent utiles comme par exemple pour calculer si l’année est bissextile, le nombre
            de jours d’un mois, etc.
            Tester la classe Date, ses attributs et ses méthodes dans une classe TestDate qui possède une méthode main().
            Exercice 1.2
        * */
    private int day, month, year;
    final static int[] DAY_IN_MONTHS = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    final static String[] daysName = {"Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"};
    final static String[] monthsName = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre",
    "Novembre", "Decembre"};
    private boolean monthsLonger = false;

    public Date(int day, int month, int year){
        if(isDate(day, month, year)){
            this.month = month;
            this.year = year;
            this.day = day;
        }else
            throw new RuntimeException("Bad Date");
    }

    public Date(){
        this(
                LocalDate.now().getDayOfMonth(),
                LocalDate.now().getMonthValue(),
                LocalDate.now().getYear()
        );
    }

    public static Date saisieDate() throws IOException {
        Date d = null;
        Scanner s = new Scanner(System.in);
        do {
            int[] date = new int[3];
            System.out.println("Entrez 3 entiers séparés par des espaces (JJ MM YYYY) : ");

            try {
                int day = s.nextInt();
                int month = s.nextInt();
                int year = s.nextInt();
                d = new Date(day, month, year);
            }catch (InputMismatchException ex){
                System.out.println("Vous n'avez pas entrez un entier.");
                s.next();
            }catch (RuntimeException e){
//                throw new RuntimeException("Date invalide. Rééssayez.\n");
                System.out.println("Date incorrect, reesayez.");
            }
        }while(d == null);
        return d;
    }

    public String toString(){
        int m = (this.month == 1)? 0 : this.month - 1;
        return daysName[this.dayOfWeek()] + " " + day + " " + monthsName[m] + " " + year + " le "+ dayOfYear() +"-ième jour de l'année";
    }

    private static boolean isDate(int day, int month, int year){
        return day >= 1 && day <= dayInMonths(month, year) && month >= 1 && month <= 12;
    }

    public void prettyPrint(){
        System.out.println();
        System.out.printf(this.toString());
    }

    private boolean isLastMonth(){
        return this.month == 12;
    }

    private void yearIncrement(){
        ++year;
    }

    private boolean lastDayOfMonth(){
        return this.day == dayInMonths(month, year);
    }

    private void monthIncrement(){
        if(!isLastMonth())
            ++month;
        else{
            month = 1;
            yearIncrement();
        }
    }

    public static boolean isLeapYear(int y){
        return y % 400 == 0 || (y % 100 != 0 && y % 4 == 0);
    }

    private boolean isLeapYear(){
        return isLeapYear(this.getYear());
    }

    public static int dayInMonths(int month, int year){
        return DAY_IN_MONTHS[month - 1] + (month == 2 && Date.isLeapYear(year)? 1 : 0);
    }

    public int dayInMonths(){ return dayInMonths(this.month, this.year); }

    public void increment(){
        if(!lastDayOfMonth()){
            ++day;
        }
        else{
            day = 1;
            monthIncrement();
        }
    }

    public int getDayOfWeek(){
        return this.dayOfWeek();
    }

    private int dayOfWeek(){
        int m = this.getMonth();
        int y = this.getYear();
        if(m == 1 || m == 2){
            m += 12;
            y--;
        }

        int k = y / 100;
        int j = y % 100;
        int h = (getDay()
                + (((m + 1) * 26) / 10)
                + j
                + (j / 4)
                + (k / 4)
                + 5 * k) % 7;

        return (h + 5) % 7;
    }

    public int dayOfYear(){
        int dayOfYear = this.getDay();
        for (int i = 1; i < getMonth(); i++) {
            dayOfYear += dayInMonths(i, getYear());
        }
        return dayOfYear;
    }

    /*
    ******* GET & SET ************
    ******************************
     */
    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        if(isDate(day, getMonth(), getYear()))
            this.day = day;
        else
            throw new RuntimeException("Incorrect value for day field");
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        if(isDate(getDay(), month, getYear()))
            this.month = month;
        else
            throw new RuntimeException("Incorrect value for month field");
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        if(isDate(getDay(), getMonth(), year))
            this.year = year;
        else
            throw new RuntimeException("Incorrect value for year field");
    }

    public void getMonthName(int w){
        if(w > 0 && w < 13)
            System.out.printf("Le " + w +"-ème mois de l'année est " + Date.monthsName[w - 1]);
        else
            throw new RuntimeException("Nombre trop grand.");
    }

    /* ************************** */

    public boolean equals(Object o){
        if(o instanceof Date){
            Date p = (Date) o;
            return this.getDay() == p.getDay() && this.getMonth() == p.getMonth() && this.getYear() == p.getYear();
        }else
            return false;
    }

    private int toUsInteger(){
      return year * 10000 + month * 100 + day;
    }

    public int compareTo(Date o){
        return this.toUsInteger() - o.toUsInteger();
    }
}
