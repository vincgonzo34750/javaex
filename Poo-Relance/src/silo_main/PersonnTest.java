package silo_main;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class PersonnTest {

    @Test
    public void testCompareAge(){
        Personn p1 = new Personn("toto", "lePelo", 12, 2, 1957);
        Personn p2 = new Personn("Helene", "laGrande", 24, 8, 1991);
        Personn p3 = new Personn("Eric", "leJeune", 4, 4, 2001);
        Personn p4 = new Personn("Erika", "LaMeme", 4, 4, 2001);

        assertTrue(p1.compareTo(p2) > 0);
        assertTrue(p3.compareTo(p4) == 0);
        assertTrue(p3.compareTo(p2) < 0);
    }

    @Test(expected = RuntimeException.class)
    public void testImpossibleBirth(){
        Personn p1 = new Personn("John", "Doe", 30, 2, 2007);
    }

    @Test
    public void testAgeNormally(){
        Personn test = new Personn("Charles", "Legros", 1, 10, 2000);
        assertEquals(21, test.getAge());
    }

    @Test
    public void testAgeWithNewFunctionAtAge(){
        Personn test = new Personn("Charles", "Legros", 1, 10, 2000);
        assertEquals(41, test.getAgeAt(new Date(1, 10, 2041)));
    }

    @Test
    public void testPersonnNotEquals(){
        Personn t1 = new Personn("C", "P", 1, 10, 2001);
        Personn t2 = new Personn("C", "p", 1, 10, 2001);
        assertFalse(t1.equals(t2));
    }

    @Test
    public void testPersonnAreEquals(){
        Personn t1 = new Personn("C", "P", 1, 10, 2001);
        Personn t2 = new Personn("C", "P", 1, 10, 2001);
        assertTrue(t1.equals(t2));
    }


    @Test
    public void testListIsSorted(){
        Personn p1 = new Personn("Eric", "Judor", 28, 11, 1982);
        Personn p2 = new Personn("Steph", "Lablao", 6, 1, 1979);
        Personn p3 = new Personn("Shidoki", "Nindo", 22, 12, 2002);
        Personn p4 = new Personn("Fadja", "Un", 2, 2, 1988);
        Personn p5 = new Personn("Matteo", "Guerrera", 14, 6, 1977);

        List<Personn> e = new ArrayList<Personn>(Arrays.asList(p4, p1, p5, p2, p3));
        List<Personn> o = new ArrayList<Personn>(Arrays.asList(p3, p4, p1, p2, p5));
        assertFalse(e.equals(o));
        Program.triList(e);
        assertTrue(e.equals(o));
    }

    @Test
    public void testListSortingWithCollection(){
        Personn p1 = new Personn("Eric", "Judor", 28, 11, 1982);
        Personn p2 = new Personn("Steph", "Lablao", 6, 1, 1979);
        Personn p3 = new Personn("Shidoki", "Nindo", 22, 12, 2002);
        Personn p4 = new Personn("Fadja", "Un", 2, 2, 1988);
        Personn p5 = new Personn("Matteo", "Guerrera", 14, 6, 1977);

        List<Personn> e = new ArrayList<Personn>(Arrays.asList(p4, p1, p5, p2, p3));
        List<Personn> o = new ArrayList<Personn>(Arrays.asList(p3, p4, p1, p2, p5));
        assertFalse(e.equals(o));
        Collections.sort(e);
        assertTrue(e.equals(o));
    }

    @Test
    public void testShufflePersonn(){
        Personn p1 = new Personn("Eric", "Judor", 28, 11, 1982);
        Personn p2 = new Personn("Steph", "Lablao", 6, 1, 1979);
        Personn p3 = new Personn("Shidoki", "Nindo", 22, 12, 2002);
        Personn p4 = new Personn("Fadja", "Un", 2, 2, 1988);
        Personn p5 = new Personn("Matteo", "Guerrera", 14, 6, 1977);

        List<Personn> e = new ArrayList<Personn>(Arrays.asList(p4, p1, p5, p2, p3));
        assertEquals("Eric", e.get(1).getFirstName());
        Program.shuffle(e);
        assertNotEquals("Eric", e.get(1).getFirstName());
    }
}
